module gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/webtunnel

go 1.18

require git.torproject.org/pluggable-transports/goptlib.git v1.2.0 // indirect
